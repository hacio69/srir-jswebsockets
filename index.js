/**
 * @module index
 */
/**
 * @class app
 * @description include express module
 */
var app = require('express')();
/**
 * @class http
 * @description include http module and create server object
 */
var http = require('http').Server(app);
/**
 * @class io
 * @description include socket.io module and send him http server
 */
var io = require('socket.io')(http);

//file saving
/**
 * @class fs
 * @description include fs(file system) module
 */
var fs = require('fs');
//new thread to test received script
//new thread to test received script
/**
 * @class childProcess
 * @description include childProcess module
 */
var childProcess = require('child_process');

var sd = require('simplediff');

var validFilesCounter = 0;

exports.scriptValidatedMsg = 'Script is correct!';
exports.scriptExecutedMsg = 'Script was successfully executed!';
exports.scriptSavedMsg = 'File has been successfully saved! ';

exports.validateScript = function (code, msg) {
    var result = code === 0 ? msg : new Error('exit code ' + code);
    return result;
}

function testScript(scriptPath, callback) {

    /**
     * @property invoked
     * @description keep track of whether callback has been invoked to prevent multiple invocations
     * @type {boolean}
     */
    var invoked = false;
    /**
     * @property precess
     * @description create new process
     */
    var process = childProcess.fork(scriptPath);

    // listen for errors as they may prevent the exit event from firing
    /**
     * @method process.on('error', function ())
     * @description listen for 'error' name event
     * @param 'error' {String} events name
     * @param function() {function} the callback function
     */
    process.on('error', function (err) {
        if (invoked) return;
        invoked = true;
        callback(err);
    });

    // execute the callback once the process has finished running
    /**
     * @method process.on('exit', function ())
     * @description listen for 'exit' name event
     * @param 'exit' {String} events name
     * @param function() {function} the callback function
     */
    process.on('exit', function (code) {
        if (invoked) return;
        invoked = true;
        var err = exports.validateScript(code, exports.scriptValidatedMsg);
        callback(err);
    });
}

/**
 * @class executeScript (scriptPath, callback)
 * @description execute the script
 * @param scriptPath {String} path to script file
 * @param callback {function} callback
 */
function executeScript(scriptPath, callback) {
    /**
     * @property isInvoked
     * @description keep track of whether callback has been invoked already
     * @type {boolean}
     */
    var isInvoked = false;
    /**
     * @property precess
     * @description create new process
     */
    var process = childProcess.fork(scriptPath);

    /**
     * @method process.on('exit', function ())
     * @description listen for 'exit' name event
     * @param 'error' {String} events name
     * @param function() {function} the callback function
     */
    process.on('exit', function (code) {
        if (isInvoked) return;
        isInvoked = true;
        var err = exports.validateScript(code, exports.scriptExecutedMsg);
        callback(err);
    });
    /**
     * @method process.on('message', function ())
     * @description listen for 'message' name event
     * @param 'message' {String} events name
     * @param function() {function} the callback function
     */
    process.on('message', function (m) {
        io.emit('chat message', '^Result: ' + m);
    });
}

exports.storeAndCompareFile = function (script, callback) {

    fs.readFile('./data/valid_files/.filescount', 'utf8', function (err, data) {
        if (err) {
            console.log(err);
            validFilesCounter = 0;
            callback('Nothing to compare with.');
            exports.saveFile(script, callback);
        } else {
            validFilesCounter = data;
            exports.getDiffWithMostSimilarScript(script, validFilesCounter, callback);
            exports.saveFile(script, callback);
        }
    });
    return validFilesCounter;
}

exports.saveFile = function (script, callback) {
    var result;
    fs.writeFile('./data/valid_files/script' + validFilesCounter + '.js', script, function (err) {
        if (err) {
            result = err;
            callback(result);
        }
        else {
            validFilesCounter++;
            result = exports.scriptSavedMsg;
            callback(result);

            fs.writeFile('./data/valid_files/.filescount', validFilesCounter, function (err) {
                if (err) result = 'Failed to save files count file!';
                else {
                    result = 'Filescount has been successfully updated!';
                }
                console.log(result);
            });
        }
    });
    return result;
}

exports.splitScripts = function (filesCount) {
    var scriptArr = [];
    for (var i = 0; i < filesCount; i++) {
        scriptArr[i] = fs.readFileSync('./data/valid_files/script' + i + '.js', "utf8").split('\n');
    }
    return scriptArr;
}

exports.diffAlgorithm = function (filesCount, scriptArr, splitedCurrent) {

    var differences = [];
    for (var i = 0; i < filesCount; i++) {
        var currentDiff = sd.diff(scriptArr[i], splitedCurrent);
        console.log(currentDiff);
        differences[i] = [0, 0];

        for (var b = 0; b < currentDiff.length; b++) {
            console.log(currentDiff.length);
            if (currentDiff[b][0] === '+') {
                differences[i][0] += currentDiff[b][1].length;
            }
            else if (currentDiff[b][0] === '-') {
                differences[i][1] += currentDiff[b][1].length;
            }
        }
    }
    return differences;
}

exports.diffSearch = function(differences){
    var diffSums = [];
    var tempDiff;
    var minDiff = [];

    for (var a = 0; a < differences.length; a++) {
        diffSums[a] = differences[a][0] + differences[a][1];   
    }

    for (var a = 0; a < diffSums.length; a++) {
        if (typeof tempDiff === 'undefined') tempDiff = diffSums[a];
        else if (diffSums[a] < tempDiff) tempDiff = diffSums[a];
    }

    for (var a = 0; a < diffSums.length; a++) {
        if (diffSums[a] === tempDiff) minDiff[minDiff.length] = a;
    }

    return minDiff;
}

exports.getDiffWithMostSimilarScript = function(currScript, filesCount, callback) {

    var splitedCurrentScript = currScript.split('\n');
    var scriptArr = [];
    var differences = [];

    scriptArr = exports.splitScripts(filesCount);

    var minDiff = [];

    differences = exports.diffAlgorithm(filesCount, scriptArr, splitedCurrentScript);
    console.log(differences);

    minDiff = exports.diffSearch(differences);
    console.log(minDiff);

    var similarFilesString = '';

    if (minDiff.length === 1) {
        similarFilesString = 'script' + minDiff[0] + '.js. - ' + differences[minDiff[0]][0] + ' insertions, ' + differences[minDiff[0]][1] + ' devarions.';
        callback('Most similar file: ' + similarFilesString);
    }
    else {
        for (var z = 0; z < minDiff.length; z++) {
            similarFilesString += 'script' + minDiff[z] + '.js.';
        }
        callback('Most similar files: ' + similarFilesString);
    }
    return similarFilesString;
}
/**
 @class app.get(path, function)
 @description send index.html directly to client
 @param '/' {String} path directory
 @param function {function} create function and passes it as a callback parameter
 */
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});
/**
 @class app.get(css styles.css, function ()
 @description send styles.css directly to client
 @param '/css/styles.css' {String} path directory
 @param function {function} create function and passes it as a callback parameter
 */
app.get('/css/styles.css', function (req, res) {
    res.sendFile(__dirname + '/css/styles.css');

});
/**
 @class app.get(js client.js, function (req, res)
 @description send client.js directly to client
 @param '/js/client.js' {String} path directory
 @param function {function} create function and passes it as a callback parameter
 */
app.get('/js/client.js', function (req, res) {
    res.sendFile(__dirname + '/js/client.js');
});
/**
 * @class io.on('connection', function())
 * @description listen for 'connection' name event
 * @param 'connection' {String} events name
 * @param function() {function} the callback function
 */
io.on('connection', function (socket) {
    socket.on('chat message', function (msg) {

        if (msg.substr(0, 8) === '<script>') {

            fs.writeFile('currScript.js', msg.substring(8, msg.length - 9));

            testScript('currScript.js', function (err) {
                io.emit('chat message', '#' + err);
                if (err === 'Script is correct!') {

                    executeScript('currScript.js', function (err) {
                        io.emit('chat message', '#' + err);

                        exports.storeAndCompareFile(msg.substring(8, msg.length - 9), function (fb) {
                            io.emit('chat message', '#' + fb);
                        });
                    });
                }
            });
        }
        else {
            io.emit('chat message', msg);
        }
    });
});
/**
 *@class http.listen(3000, function ())
 *@description Function with allow to listen on specified port (3000)
 *@param 3000 {Number} Port number
 *@param function() {console.log('listening on *:3000');}
 */
http.listen(3000, function () {
    console.log('listening on *:3000');
});

