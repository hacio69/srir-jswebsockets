/**
 * @module client
 */
/**
 * @class function
 * @description allows client to send message and receive response from server
 */$(function () {
    /**
     * @property socket
     * @description allows client to connect to server
     */
    var socket = io();
    /**
     * @method $('form').submit(function())
     * @description take form and make callback
     */
    $('form').submit(function () {
        socket.emit('chat message', $('#m').val());
      //  $('#m').val('');
        return false;
    });
    /**
     * @method socket.on('chat message', function ())
     * @description listen for 'chat message' name event
     * @param 'chat_message' {String} events name
     * @param function() {function} the callback function
     */
    socket.on('chat message', function (msg) {

        if (msg.charAt(0) === '<') {

            var newdiv = document.createElement('div');

            newdiv.innerHTML = msg;

            document.getElementById('results').append(newdiv);
        }
        else if (msg.charAt(0) === '#') $('#messages').append($('<li class="server-msg">').text(msg));
        else if (msg.charAt(0) === '^') $('#messages').append($('<li class="server-result-msg">').text(msg));
        else $('#messages').append($('<li>').text(msg));
    });

    $('#m').on('keypress', function (e) {
        if (e.shiftKey && e.keyCode == 13) {
            e.preventDefault();
            $('button').click();
        }
    });
});
