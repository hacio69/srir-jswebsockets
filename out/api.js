YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "app",
        "app.get(css styles.css, function ()",
        "app.get(js client.js, function (req, res)",
        "app.get(path, function)",
        "childProcess",
        "executeScript (scriptPath, callback)",
        "fs",
        "function",
        "http",
        "http.listen(3000, function ())",
        "io",
        "io.on('connection', function())",
        "testScript (scriptPath, callback)"
    ],
    "modules": [
        "client",
        "index"
    ],
    "allModules": [
        {
            "displayName": "client",
            "name": "client"
        },
        {
            "displayName": "index",
            "name": "index"
        }
    ],
    "elements": []
} };
});