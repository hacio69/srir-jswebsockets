let expect = require("chai").expect;
let request = require("request");
var index = require('../index');
let current = require('../currScript');
let fs = require('fs');
var assert = require("chai").assert;

describe('first test',function () {

    let viableScript = "data/viable-script.js";
    let invalidScript = "data/invalid-script.js";

    it("validate-should-return-success",function () {
        var result = index.validateScript(0,index.scriptValidatedMsg);
        expect(result).to.equal(index.scriptValidatedMsg);
    })

    it("validate-should-return-error",function () {
        var code = 12;
        var result = index.validateScript(code,'');
        var exptectedResult = new Error('exit code ' + code);
        expect(result.message).to.equal(exptectedResult.message);
    })

    it("validate-load-filesCount-function",function(){
        var result = index.storeAndCompareFile(viableScript,function(){});
        expect(result).to.be.a('number');
    })

    it("validate-correct-filesCount-function",function(){
        var result = index.storeAndCompareFile(viableScript,function(){});
        expect(result).to.be.at.least(0);
    })

    it("validate-save-function",function(){
        var result = index.saveFile(viableScript,function(){});
        expect(result).to.not.be.an('error');
    })

    it("validate-split-script",function(){
        var filesCount = 10;
        var result = index.splitScripts(filesCount);
        expect(result).to.be.an('array');
    })

    it("validate-split-script-lenght",function(){
        var filesCount = 10;
        var result = index.splitScripts(filesCount);
        expect(result).to.have.lengthOf(filesCount);
    })

    it('validate-diffAlgorithm-function', function(){
        var filesCount = 10;
        var scriptArr = index.splitScripts(filesCount);
        var splitedCurrent = viableScript.split('\n');
        var result = index.diffAlgorithm(filesCount,scriptArr,splitedCurrent);
  
        for(let i=0;i<result.length;i++)
            expect(result[i][0]).to.be.at.least(0);
    })

    it('validate-diffSearch-function',function(){
        var filesCount = 10;
        var scriptArr = index.splitScripts(filesCount);
        var splitedCurrent = viableScript.split('\n');
        var differences = index.diffAlgorithm(filesCount,scriptArr,splitedCurrent);
  
        var result = index.diffSearch(differences);
        expect(result).to.not.be.null;
        expect(result.length).to.be.above(0);
    })

    it('validate-whole-algorithm-function',function(){
        var result = index.getDiffWithMostSimilarScript(viableScript,20,function(){});
        expect(result).to.not.be.null;
        expect(result).to.include('script');
    })

/*    it("validate-should-return-success",function () {
        var callback = function(err){
            return expect(err).to.equal('asd');
        }
        return index.validateScript(viableScript,callback).then(
            expect(callback()).to.equal('asd')
        );
    })

    it("validate-should-return-error",function () {
        var callback = function(err){
            expect(typeof(err)).to.equal(typeof(Error));
        }
        index.validateScript(invalidScript,callback);
    })
*/
})